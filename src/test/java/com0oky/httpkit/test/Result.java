/**
 * @author mdc
 * @date 2017年7月5日 下午4:43:48
 */
package com0oky.httpkit.test;

import java.util.Arrays;

/**
 * @author mdc
 * @date 2017年7月5日 下午4:43:48
 */
public class Result {
	
	private Accurate[] accurate;

	/**
	 * @return 获取{@link #accurate}
	 */
	public Accurate[] getAccurate() {
		return accurate;
	}

	/**
	 * @param accurate 设置accurate
	 */
	public void setAccurate(Accurate[] accurate) {
		this.accurate = accurate;
	}

	@Override
	public String toString() {
		return "Result [accurate=" + Arrays.toString(accurate) + "]";
	}
	
}
